﻿#include <iostream>
#include <string>

int main()
{
	std::cout << "Even and odd numbers from 0 to 35. Press 'even' to even numbers. Press any key for odd numbers: " << "\n";
	std::string NumberEven;
	std::cin >> NumberEven;

	const int n = 35;

	if (NumberEven == "even")
	{
		for (int a = 0; a <= n; a++)
		{
			std::cout << "even" << " " << a++ << "\n";
		}
	}
	else
	{
		for (int b = 1; b <= n; b++)
		{
			std::cout << "odd" << " " << b++ << "\n";
		}
	}
	system("pause");
}